//@ts-check
"use strict";
const gulp = require("gulp");
const { buildLib } = require("./gulp.lib")

gulp.task("default", async (done) => {
  console.info(`\n\n👷 BUILDING LIB 👷\n`);
  console.info(`========================\n`);
  await buildLib();
  console.info(`------------------------\n`);
  console.info(`\n🙌 LIB Done!!! 🙌\n\n`);
  done();
});
