#!/bin/bash

npm pack
mkdir _stage
mv woodchuck-*.tgz _stage
cd _stage
npm init -y
tar xf woodchuck-*.tgz
rm woodchuck-*.tgz
mkdir node_modules
mv package node_modules/woodchuck

echo "
const log = require('woodchuck')

console.log(log)
log.info('echo 1')
log.persist({ foo: 'bar' })
log.info({ details: 'test2' })
log.info('echo 2')
log.persistClear()
" > index.js
echo "
import * as log from 'woodchuck'

log.config({ logFormat: log.format.msgJson })
log.info('Test msgJson')
log.config({ logFormat: log.format.awsNode })
log.info('Test aws')
log.config({ logFormat: log.format.json })
log.info('Test json')

console.log('---------------')

log.config({ logFormat: log.format.msgJson })
log.info('Test')

log.config({ logCallback: () => {
  console.log('You called?')
} })

log.info('Answer the call!')
log.nr('MyFunc', 'NR Log Attempt')
log.persist({ one: 'two' })
log.info('Test')
" > index.ts

ts-node index.ts  && node index.js && sleep 2 && code .
