//@ts-check
"use strict"

// Gulp
const gulp = require("gulp")
const uglify = require("gulp-uglify")
const babel = require("gulp-babel")
const path = require("path")

// Dirs
const dirRoot = path.resolve(__dirname, "..")
const dirSrc = path.join(dirRoot, "src")
const dirLib = path.join(dirRoot, "lib")

module.exports.buildLib = async () => {
  return new Promise((res) =>
    gulp //
      .src(path.join(dirSrc, "index.js"))
      .pipe(
        babel({
          presets: ["@babel/env"],
          plugins: ["@babel/plugin-transform-modules-commonjs"],
        })
      )
      .pipe(uglify())
      .on("error", console.error)
      .pipe(gulp.dest(dirLib))
      .on("end", () => {
        console.info(`Finished building`)
        res()
      })
  )
}
