// Project
const log = require("../index")

describe("NR Logging", () => {
  it("should invoke New Relic addPageAction", () => {
    // Arrange
    const nrSpy = jest.fn()
    window.newrelic = {
      addPageAction: nrSpy,
    }
    console.info.mockImplementation(() => {})
    // Act
    log.nr("myFunc", "myAction", { status: "failure" })
    // Assert
    expect(nrSpy).toBeCalledWith("myAction", { status: "failure", functionName: "myFunc" })
    expect(console.info).toBeCalledWith("NR: myAction @myFunc()", { status: "failure" })
    delete window.newrelic
  })
  it("should graceful error log with no New Relic", () => {
    // Arrange
    console.error.mockImplementation(() => {})
    // Act
    log.nr("myFunc", "myAction", { status: "failure" })
    // Assert
    expect(console.error).toBeCalledWith(
      "Error logging to New Relic: 'Cannot read property 'addPageAction' of undefined'"
    )
  })
})
