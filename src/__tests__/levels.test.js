// Project
const log = require("../index")
const { firstCallFirstArg } = require("./testUtils")

describe("Log Levels", () => {
  describe("debug()", () => {
    it("should log debugs", () => {
      // Arrange
      console.debug.mockImplementation(() => {})
      // Act

      log.config({ minLogLevel: log.level.debug })
      log.debug("My debug log")
      // Assert
      expect(firstCallFirstArg(console.debug)).toEqual("My debug log")
    })

    it("should not log debugs", () => {
      // Arrange
      console.debug.mockImplementation(() => {})
      // Act
      log.config({ minLogLevel: log.level.info })
      log.debug("My debug log")
      // Assert
      expect(firstCallFirstArg(console.debug)).toBeUndefined()
    })
  })

  describe("info()", () => {
    it("should log infos", () => {
      // Arrange
      console.info.mockImplementation(() => {})
      // Act
      log.config({ minLogLevel: log.level.info })
      log.info("My info log")
      // Assert
      expect(firstCallFirstArg(console.info)).toEqual("My info log")
    })

    it("should not log infos", () => {
      // Arrange
      console.info.mockImplementation(() => {})
      // Act
      log.config({ minLogLevel: log.level.warn })
      log.info("My info log")
      // Assert
      expect(firstCallFirstArg(console.info)).toBeUndefined()
    })
  })

  describe("warn()", () => {
    it("should log warns", () => {
      // Arrange
      console.warn.mockImplementation(() => {})
      // Act
      log.config({ minLogLevel: log.level.warn })
      log.warn("My warn log")
      // Assert
      expect(firstCallFirstArg(console.warn)).toEqual("My warn log")
    })
    it("should not log warns", () => {
      // Arrange
      console.warn.mockImplementation(() => {})
      // Act
      log.config({ minLogLevel: log.level.error })
      log.warn("My warn log")
      // Assert
      expect(firstCallFirstArg(console.warn)).toBeUndefined()
    })
  })

  describe("error()", () => {
    it("should log errors", () => {
      // Arrange
      console.error.mockImplementation(() => {})
      // Act
      log.config({ minLogLevel: log.level.error })
      log.error("My error log")
      // Assert
      expect(firstCallFirstArg(console.error)).toEqual("My error log")
    })
    it("should not log errors", () => {
      // Arrange
      console.error.mockImplementation(() => {})
      // Act
      log.config({ minLogLevel: log.level.fatal })
      log.error("My error log")
      // Assert
      expect(firstCallFirstArg(console.error)).toBeUndefined()
    })
  })

  describe("fatal()", () => {
    it("should always log fatals", () => {
      // Arrange
      console.error.mockImplementation(() => {})
      // Act
      log.config({ minLogLevel: log.level.fatal })
      log.fatal("My fatal log")
      // Assert
      expect(console.error.mock.calls[0][0]).toEqual("My fatal log")
    })
  })
})
