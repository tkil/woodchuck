// Project
const log = require("../index")
const { firstCallSecondArg } = require("./testUtils")

describe("Config Functions", () => {
  beforeAll(() => {
    log.config({ minLogLevel: log.level.debug })
  })

  beforeEach(() => {
    console.info.mockImplementation(() => {})
    log.config({})
  })

  describe("set AppName", () => {
    it("should log app name with payload", () => {
      log.config({ appName: "My App" })
      log.info("My log")
      // Assert
      const expected = { appName: "My App", level: log.level.info }
      expect(firstCallSecondArg(console.info)).toEqual(expected)
    })
    it("should remove the app name", () => {
      log.config({ appName: "My App" })
      log.config({})
      log.info("My log")
      // Assert
      const expected = { level: log.level.info }
      expect(firstCallSecondArg(console.info)).toEqual(expected)
    })
  })

  describe("set LogDebugFn", () => {
    it("should use info instead of debug", () => {
      log.config({ logDebugFn: console.info })
      log.debug("My debug log")
      // Assert
      expect(console.info).toBeCalledTimes(1)
      expect(console.debug).not.toBeCalled()
    })
  })

  describe("setLogMaxTotalChars()", () => {
    it("should limit the max characters in the log", () => {
      // Arrange
      const myLimit = 200
      const msgChars = myLimit + 20
      let msg = ""
      for (let i = 0; i < msgChars; i++) {
        msg += "a"
      }
      // Act
      log.config({ logMaxTotalChars: myLimit })
      log.info(msg)
      // Assert
      const length = console.info.mock.calls[0].join("").length
      expect(length).toBeLessThan(myLimit)
    })
    it("should limit the max characters in the log via setOptions()", () => {
      // Arrange
      console.info.mockImplementation(() => {})
      const myLimit = 200
      const msgChars = myLimit + 20
      let msg = ""
      for (let i = 0; i < msgChars; i++) {
        msg += "a"
      }
      // Act
      log.config({ logMaxTotalChars: myLimit })
      log.info(msg)
      // Assert
      const length = console.info.mock.calls[0].join("").length
      expect(length).toBeLessThan(myLimit)
    })
    it("should NOT limit the max characters in the log", () => {
      // Arrange
      console.info.mockImplementation(() => {})
      const fakeLimit = 200
      const msgChars = 2000
      let msg = ""
      for (let i = 0; i < msgChars; i++) {
        msg += "a"
      }
      // Act
      log.config({ logMaxTotalChars: fakeLimit })
      log.config({})
      log.info(msg)
      // Assert
      const length = console.info.mock.calls[0].join("").length
      expect(length).toBeGreaterThan(fakeLimit)
    })
  })
})
