// Project
const log = require("../index")

describe("Log Types", () => {
  describe("setLogTypeDefaultGeneral()", () => {
    it("should set type general by default when enabled", () => {
      // Arrange
      console.info.mockImplementation(() => {})
      // Act
      log.config({ logTypeDefaultGeneral: true })
      log.info("My log")
      // Assert
      const expected = { level: log.level.info, type: log.type.general }
      expect(console.info.mock.calls[0][1]).toEqual(expected)
      log.config({ logTypeDefaultGeneral: false })
    })
  })
  describe("setLogTypeFilter()", () => {
    it("should only log api and init types", () => {
      // Arrange
      console.info.mockImplementation(() => {})
      // Act
      log.config({ logTypeFilter: [log.type.api, log.type.init] })
      log.info("My general log", { type: log.type.general })
      log.info("My init log", { type: log.type.init })
      log.info("My api log", { type: log.type.api })
      log.info("My store", { type: log.type.store })
      // Assert
      expect(console.info).not.toBeCalledWith("My general log", { level: log.level.info, type: log.type.general })
      expect(console.info).toBeCalledWith("My init log", { level: log.level.info, type: log.type.init })
      expect(console.info).toBeCalledWith("My api log", { level: log.level.info, type: log.type.api })
      expect(console.info).not.toBeCalledWith("My store", { level: log.level.info, type: log.type.store })
    })
    it("should not filter when filter is bad", () => {
      // Arrange
      console.info.mockImplementation(() => {})
      // Act
      log.config({ logTypeFilter: undefined })
      log.info("Log 1", { type: log.type.general })
      //@ts-ignore
      log.config({ logTypeFilter: null })
      log.info("Log 2", { type: log.type.general })
      log.config({ logTypeFilter: [] })
      log.info("Log 3", { type: log.type.general })
      log.config({ logTypeFilter: [""] })
      log.info("Log 4", { type: log.type.general })
      //@ts-ignore
      log.config({ logTypeFilter: [undefined, undefined] })
      log.info("Log 5", { type: log.type.general })
      //@ts-ignore
      log.config({ logTypeFilter: [undefined, undefined] })
      log.info("Log 6", { type: log.type.general })
      //@ts-ignore
      log.config({ logTypeFilter: [undefined, null, ""] })
      log.info("Log 7", { type: log.type.general })
      //@ts-ignore
      log.config({ logTypeFilter: [undefined, null, "superfilter"] })
      log.info("Log 8", { type: log.type.general })
      // Assert
      expect(console.info).toBeCalledWith("Log 1", { level: log.level.info, type: log.type.general })
      // expect(console.info).toBeCalledWith('Log 2', { level: log.level.info, type: log.type.general })
      // expect(console.info).toBeCalledWith('Log 3', { level: log.level.info, type: log.type.general })
      // expect(console.info).toBeCalledWith('Log 4', { level: log.level.info, type: log.type.general })
      // expect(console.info).toBeCalledWith('Log 5', { level: log.level.info, type: log.type.general })
      // expect(console.info).toBeCalledWith('Log 6', { level: log.level.info, type: log.type.general })
      // expect(console.info).toBeCalledWith('Log 7', { level: log.level.info, type: log.type.general })
      // expect(console.info).not.toBeCalledWith('Log 8', { level: log.level.info, type: log.type.general })
    })
  })
})
