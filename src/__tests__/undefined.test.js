// Project
const log = require("../index")

describe("Logging Undefined", () => {
  describe("undefined()", () => {
    it("should safely catch when window.console is not defined", (done) => {
      // Arrange
      console.info.mockImplementation(() => {})
      const consoleRestore = window.console
      Object.assign(window, { console: undefined })
      // Act
      log.info("I should work")
      // Assert
      try {
        done()
      } catch (error) {
        done.fail("Error when calling log.info")
      } finally {
        Object.assign(window, { console: consoleRestore })
      }
    })
  })
})
