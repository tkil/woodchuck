// Project
const log = require("../index")
const { firstCallFirstArg } = require("./testUtils")

describe("Logging Functions", () => {
  describe("debug()", () => {
    it("should log to debug", () => {
      // Arrange
      console.debug.mockImplementation(() => {})
      // Act
      log.debug("My debug log")
      // Assert
      expect(firstCallFirstArg(console.debug)).toEqual("My debug log")
    })
  })
  describe("info()", () => {
    it("should log to info", () => {
      // Arrange
      console.info.mockImplementation(() => {})
      // Act
      log.info("My info log")
      // Assert
      expect(firstCallFirstArg(console.info)).toEqual("My info log")
    })
  })
  describe("warn()", () => {
    it("should log to warn", () => {
      // Arrange
      console.warn.mockImplementation(() => {})
      // Act
      log.warn("My warn log")
      // Assert
      expect(firstCallFirstArg(console.warn)).toEqual("My warn log")
    })
  })
  describe("error()", () => {
    it("should log to error", () => {
      // Arrange
      console.error.mockImplementation(() => {})
      // Act
      log.error("My error log")
      // Assert
      expect(firstCallFirstArg(console.error)).toEqual("My error log")
    })
  })
  describe("fatal()", () => {
    it("should log to fatal", () => {
      // Arrange
      console.error.mockImplementation(() => {})
      // Act
      log.fatal("My fatal log")
      // Assert
      expect(firstCallFirstArg(console.error)).toEqual("My fatal log")
    })
  })
})
