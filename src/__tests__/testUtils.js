module.exports.firstCallFirstArg = (m) => {
  try {
    return m.mock.calls[0][0]
  } catch (err) {
    return undefined
  }
}

module.exports.firstCallSecondArg = (m) => {
  try {
    return m.mock.calls[0][1]
  } catch (err) {
    return undefined
  }
}
