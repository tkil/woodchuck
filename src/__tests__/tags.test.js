// Project
const log = require("../index")

describe("Log Tags", () => {
  describe("setLogTagFilter()", () => {
    it("should only log a and c tags", () => {
      // Arrange
      console.info.mockImplementation(() => {})
      // Act
      log.config({ logTagFilter: ["a", "c"] })
      log.info("a", { tags: ["a"] })
      log.info("ab", { tags: ["a", "b"] })
      log.info("ac", { tags: ["a", "c"] })
      log.info("b", { tags: ["b"] })
      log.info("bd", { tags: ["b", "d"] })
      // Assert
      expect(console.info).toBeCalledWith("a", { level: log.level.info, tags: ["a"] })
      expect(console.info).toBeCalledWith("ab", { level: log.level.info, tags: ["a", "b"] })
      expect(console.info).toBeCalledWith("ac", { level: log.level.info, tags: ["a", "c"] })
      expect(console.info).not.toBeCalledWith("b", { level: log.level.info, tags: ["b"] })
      expect(console.info).not.toBeCalledWith("bd", { level: log.level.info, tags: ["b", "d"] })
    })
    it("should not filter when filter is bad", () => {
      // Arrange
      console.info.mockImplementation(() => {})
      // Act
      log.config({ logTagFilter: undefined })
      log.info("Log 1", { tags: ["a"] })
      //@ts-ignore
      log.config({ logTagFilter: null })
      log.info("Log 2", { tags: ["a"] })
      log.config({ logTagFilter: [] })
      log.info("Log 3", { tags: ["a"] })
      log.config({ logTagFilter: [""] })
      log.info("Log 4", { tags: ["a"] })
      //@ts-ignore
      log.config({ logTagFilter: [undefined, undefined] })
      log.info("Log 5", { tags: ["a"] })
      //@ts-ignore
      log.config({ logTagFilter: [undefined, undefined] })
      log.info("Log 6", { tags: ["a"] })
      //@ts-ignore
      log.config({ logTagFilter: [undefined, null, ""] })
      log.info("Log 7", { tags: ["a"] })
      //@ts-ignore
      log.config({ logTagFilter: [undefined, null, "superfilter"] })
      log.info("Log 8", { tags: ["a"] })
      // Assert
      expect(console.info).toBeCalledWith("Log 1", { level: log.level.info, tags: ["a"] })
      expect(console.info).toBeCalledWith("Log 2", { level: log.level.info, tags: ["a"] })
      expect(console.info).toBeCalledWith("Log 3", { level: log.level.info, tags: ["a"] })
      expect(console.info).toBeCalledWith("Log 4", { level: log.level.info, tags: ["a"] })
      expect(console.info).toBeCalledWith("Log 5", { level: log.level.info, tags: ["a"] })
      expect(console.info).toBeCalledWith("Log 6", { level: log.level.info, tags: ["a"] })
      expect(console.info).toBeCalledWith("Log 7", { level: log.level.info, tags: ["a"] })
      expect(console.info).not.toBeCalledWith("Log 8", { level: log.level.info, tags: ["a"] })
    })
  })
})
