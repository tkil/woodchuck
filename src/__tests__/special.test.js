// Project
const log = require("../index")

describe("Special Functions", () => {
  beforeEach(() => {
    log.config({})
  })
  describe("persist()", () => {
    it("should log persistent payload", () => {
      // Arrange
      console.info.mockImplementation(() => {})
      // Act
      log.persist({ foo: "bar" })
      log.info("My log")
      // Assert
      const expected = { foo: "bar", level: log.level.info }
      expect(console.info.mock.calls[0][1]).toEqual(expected)
      log.persistClear()
    })
  })
  describe("setLogCallback()", () => {
    it("should invoke the log callback", () => {
      // Arrange
      console.info.mockImplementation(() => {})
      const spy = jest.fn()
      // Act
      log.config({ logCallback: spy })
      log.info("My log")
      // Assert
      const expected = { message: "My log", level: log.level.info }
      expect(spy).toHaveBeenCalledWith(expected)
    })
    it("should have message in payload", () => {
      // Arrange
      console.info.mockImplementation(() => {})
      const spy = jest.fn()
      // Act
      log.config({ logCallback: spy })
      log.info("My log")
      // Assert
      const expected = { message: "My log", level: log.level.info }
      expect(spy).toHaveBeenCalledWith(expected)
    })
    it("should remove the log callback", () => {
      // Arrange
      console.info.mockImplementation(() => {})
      const spy = jest.fn()
      log.config({ logCallback: spy })
      // Act
      log.config({})
      log.info("My log")
      // Assert
      expect(spy).not.toHaveBeenCalled()
    })
  })
})
