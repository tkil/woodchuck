export type IObject = { [index: string]: any }
export type LogFormat = "aws-node" | "json" | "msg-json" | "msg-lf-str-json"
export type LogLevel = "FATAL" | "ERROR" | "WARN" | "INFO" | "DEBUG"
export type LogType = string

export type ConsoleFn = (message?: any, ...optionalParams: any[]) => void
export type LogNr = (functionName: string, actionName: string, attributes?: object) => void
export type LogCallback = (logPayload: ILogPayload) => void

export type LogFnInternal = (payload: ILogPayload) => void

export interface ILoggerOptions {
  appName?: string
  logFormat?: LogFormat
  logCallback?: LogCallback
  uuid?: boolean
  logTypeDefaultGeneral?: boolean
  persist?: IObject
  logDebugFn?: LogFnInternal
  logInfoFn?: LogFnInternal
  logWarnFn?: LogFnInternal
  logErrorFn?: LogFnInternal
  logFatalFn?: LogFnInternal
  minLogLevel?: LogLevel
  logMaxTotalChars?: number
  logTagFilter?: string[]
  logTypeFilter?: LogType[]
}

export interface ILogPayload {
  data?: object
  details?: string
  event?: string | object
  function?: string
  level?: LogLevel
  location?: string
  message?: string
  result?: string
  status?: string
  tags?: string[]
  type?: string
  userMessage?: string
  error?: Error | string
}

export type LogFunction = (message: string | ILogPayload, payload?: ILogPayload) => void

// {
//   /**
//    * @param {ILoggerOptions} options
//    */
//   config: (options) => {
//     _appName = options.appName ? options.appName : undefined
//     _logFormat = options.logFormat ? options.logFormat : logFormat.msgJson
//     _logCallback = options.logCallback ? options.logCallback : undefined
//     _uuid = options.uuid ? uuid() : undefined
//     _minLogLevel = toLogLevelNum(options.minLogLevel ? options.minLogLevel : defaultMinLogLevel)
//     _logMaxTotalChars = options.logMaxTotalChars ? options.logMaxTotalChars : undefined
//     _logTagFilter = options.logTagFilter ? options.logTagFilter : undefined
//     _logTypeFilter = options.logTypeFilter ? options.logTypeFilter : undefined
//     _logTypeDefaultGeneral = options.logTypeDefaultGeneral ? options.logTypeDefaultGeneral : false
//     //
//     _fatal = options.logFatalFn ? options.logFatalFn : consoleActual.error
//     _error = options.logErrorFn ? options.logErrorFn : consoleActual.error
//     _warn = options.logWarnFn ? options.logWarnFn : consoleActual.warn
//     _info = options.logInfoFn ? options.logInfoFn : consoleActual.info
//     _debug = options.logDebugFn ? options.logDebugFn : consoleActual.debug
//   },
//   /** @type {LogFunction} */
//   fatal: (message, payload) => {
//     logCall(logLevel.fatal, message, payload)
//   },
//   /** @type {LogFunction} */
//   error: (message, payload) => {
//     logCall(logLevel.error, message, payload)
//   },
//   /** @type {LogFunction} */
//   warn: (message, payload) => {
//     logCall(logLevel.warn, message, payload)
//   },
//   /** @type {LogFunction} */
//   info: (message, payload) => {
//     logCall(logLevel.info, message, payload)
//   },
//   /** @type {LogFunction} */
//   debug: (message, payload) => {
//     logCall(logLevel.debug, message, payload)
//   },
//   nr: newRelic,
//   newRelic: newRelic,
//   // Utils
//   /**
//    * @param {IObject} object
//    */
//   persist: (object) => {
//     _persistent = { ..._persistent, ...object }
//   },
//   persistClear: () => {
//     _persistent = {}
//   },
//   // Enums
//   format: logFormat,
//   level: logLevel,
//   status: logStatus,
//   type: logType,
// }
