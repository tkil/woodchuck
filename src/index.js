/**
 * @typedef {import("./types").ConsoleFn} ConsoleFn
 * @typedef {import("./types").ILogPayload} ILogPayload
 * @typedef {import("./types").IObject} IObject
 * @typedef {import("./types").LogCallback} LogCallback
 * @typedef {import("./types").LogNr} LogNr
 * @typedef {import("./types").LogFormat} LogFormat
 * @typedef {import("./types").LogLevel} LogLevel
 * @typedef {import("./types").LogType} LogType
 * @typedef {import("./types").ILogger} ILogger
 * @typedef {import("./types").ILoggerOptions} ILoggerOptions
 * @typedef {import("./types").LogFunction} LogFunction
 */

const logMaxTotalCharsLowerLimit = 100
const defaultMinLogLevel = "DEBUG"

const logLevel = {
  fatal: /** @type {LogLevel} */ ("FATAL"),
  error: /** @type {LogLevel} */ ("ERROR"),
  warn: /** @type {LogLevel} */ ("WARN"),
  info: /** @type {LogLevel} */ ("INFO"),
  debug: /** @type {LogLevel} */ ("DEBUG"),
}

const logLevelNum = {
  fatal: 0,
  error: 1,
  warn: 2,
  info: 3,
  debug: 4,
}

const logStatus = {
  conflict: "CONFLICT",
  empty: "EMPTY",
  failure: "FAILURE",
  incomplete: "INCOMPLETE",
  missing: "MISSING",
  notFound: "NOT_FOUND",
  quotaExceeded: "QUOTA_EXCEEDED",
  success: "SUCCESS",
  timeout: "TIMEOUT",
  tooManyRequests: "TOO_MANY_REQUESTS",
  unauthorized: "UNAUTHORIZED",
}

const logType = {
  api: "API",
  general: "GENERAL",
  init: "INIT",
  io: "IO",
  mock: "MOCK",
  network: "NETWORK",
  processing: "PROCESSING",
  render: "RENDER",
  store: "STORE",
  transform: "TRANSFORM",
}

const logFormat = {
  awsNode: /** @type {LogFormat} */ ("aws-node"),
  msgJson: /** @type {LogFormat} */ ("msg-json"),
  msgLfStrJson: /** @type {LogFormat} */ ("msg-lf-str-json"),
  json: /** @type {LogFormat} */ ("json"),
}

const consoleActual =
  typeof console !== "undefined"
    ? console
    : {
        debug: () => {},
        info: () => {},
        warn: () => {},
        error: () => {},
      } // Fallback for IE

// Utils
//////////////////////////////////////////////////
/** @type {LogLevel} */

/**
 * @param {LogLevel} str
 */
const toLogLevelNum = (str) => {
  switch (str.toUpperCase()) {
    case logLevel.fatal:
      return logLevelNum.fatal
    case logLevel.error:
      return logLevelNum.error
    case logLevel.warn:
      return logLevelNum.warn
    case logLevel.info:
      return logLevelNum.info
    case logLevel.debug:
      return logLevelNum.debug
  }
  return logLevelNum.info
}

/**
 * @param {object} payload
 * @param {number} limit
 */
const trimLogPayload = (payload, limit) => {
  const trimVal = (r, val) => {
    const s = typeof val === "object" ? JSON.stringify(val) : `${val}`
    const l = s.length
    r.n = r.n - l
    return r.n >= 0 ? val : s.slice(0, r.n + l)
  }
  const _p = {}
  const keys = Object.keys(payload)
  const ref = { n: limit > logMaxTotalCharsLowerLimit ? limit : logMaxTotalCharsLowerLimit }
  trimVal(ref, keys)
  for (let i = 0; i < keys.length; i++) {
    _p[keys[i]] = trimVal(ref, payload[keys[i]])
  }
  return _p
}

const uuid = () => {
  const dt = new Date().getTime()
  return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, (c) => {
    const r = (dt + Math.random() * 16) % 16 | 0
    return (c === "x" ? r : (r & 0x3) | 0x8).toString(16)
  })
}

// Internal Config
//////////////////////////////////////////////////
/** @type {string | undefined} */
let _appName

/** @type {LogCallback | undefined} */
let _logCallback

/** @type {LogFormat} */
let _logFormat = logFormat.msgJson

/** @type {number | undefined} */
let _logMaxTotalChars

/** @type {LogType[] | undefined} */
let _logTagFilter

/** @type {boolean} */
let _logTypeDefaultGeneral

/** @type {LogType[] | undefined} */
let _logTypeFilter

/** @type {number} */
let _minLogLevel = toLogLevelNum(defaultMinLogLevel)

/** @type {IObject} */
let _persistent = {}

/** @type {string | undefined} */
let _uuid

// Internal Log Methods
//////////////////////////////////////////////////
/** @type {ConsoleFn} */ let _fatal = consoleActual.error
/** @type {ConsoleFn} */ let _error = consoleActual.error
/** @type {ConsoleFn} */ let _warn = consoleActual.warn
/** @type {ConsoleFn} */ let _info = consoleActual.info
/** @type {ConsoleFn} */ let _debug = consoleActual.debug

/** @type {LogNr} */
let _newRelic = (functionName, actionName, attributes) => {
  try {
    //@ts-ignore
    window.newrelic.addPageAction(actionName, {
      ...attributes,
      ...{ functionName },
    })
    consoleActual.info(`NR: ${actionName} @${functionName}()`, attributes)
  } catch (err) {
    consoleActual.error(`Error logging to New Relic: '${err.message}'`)
  }
}

// Internal Functions
//////////////////////////////////////////////////
/**
 * @param {LogLevel} level
 * @param {string | ILogPayload} message
 * @param {ILogPayload=} payload
 * @return {ILogPayload}
 */
const consolidatePayload = (level, message, payload) => {
  const persistValues = /** @type {IObject} */ ({})
  for (const key of Object.keys(_persistent)) {
    if (typeof _persistent[key] === "function") {
      persistValues[key] = _persistent[key]()
    } else {
      persistValues[key] = _persistent[key]
    }
  }
  try {
    //@ts-ignore
    if (typeof payload.error.message === "string") {
      //@ts-ignore
      payload.error = payload?.error?.message
    }
  } catch (_) {}

  const _payload = {
    ...(_uuid ? { uuid: _uuid } : {}),
    level,
    ...(typeof message === "string" ? { message } : message),
    ...(_logTypeDefaultGeneral ? { type: logType.general } : {}),
    ...persistValues,
    ...payload,
    ...(_appName ? { appName: _appName } : {}),
  }
  return _logMaxTotalChars ? trimLogPayload(_payload, _logMaxTotalChars) : _payload
}

/**
 * @param {LogLevel} level
 * @param {string | ILogPayload} message
 * @param {ILogPayload=} payload
 */
const logCall = (level, message, payload) => {
  const _payload = consolidatePayload(level, message, payload)
  /**
   * @param {(...args: any[]) => void} fn
   * @param {ILogPayload} p
   */
  const fmt = (fn, p) => {
    try {
      switch (_logFormat) {
        case logFormat.json:
          return fn(p)
        case logFormat.awsNode:
          return safeStdWrite(`${JSON.stringify(p)}`)
        case logFormat.msgLfStrJson:
          return fn(`${p.message}\n${JSON.stringify(p)}`)
        case logFormat.msgJson: {
          const pNoMsg = { ...p }
          delete pNoMsg.message
          return fn(p.message || "", pNoMsg)
        }
      }
    } catch (error) {
      return undefined
    }
  }

  if (
    Array.isArray(_logTypeFilter) &&
    _logTypeFilter.find((f) => Boolean(f)) !== undefined &&
    _logTypeFilter.indexOf(_payload.type || "") === -1
  ) {
    return
  }
  if (
    Array.isArray(_logTagFilter) &&
    _logTagFilter.find((f) => Boolean(f)) !== undefined &&
    !(_payload.tags || []).find((tag) => tag === _logTagFilter.find((tf) => tf === tag))
  ) {
    return
  }

  if (_minLogLevel < toLogLevelNum(level ?? defaultMinLogLevel)) {
    return
  }

  switch (level) {
    case logLevel.fatal:
      fmt(_fatal, { error: new Error(_payload.message), ..._payload })
      break
    case logLevel.error:
      fmt(_error, { error: new Error(_payload.message), ..._payload })
      break
    case logLevel.warn:
      fmt(_warn, _payload)
      break
    case logLevel.info:
      fmt(_info, _payload)
      break
    case logLevel.debug:
      fmt(_debug, _payload)
      break
  }
  if (_logCallback) {
    _logCallback({ ..._payload })
  }
}

/**
 * @param {string} s
 */
const safeStdWrite = (s) => {
  try {
    // @ts-ignore
    process.stdout.write(s + "\n")
  } catch (err) {
    _info(s)
  }
}

/**
 * @param {string} functionName
 * @param {string} actionName
 * @param {IObject=} attributes
 */
const newRelic = (functionName, actionName, attributes) => {
  _newRelic(functionName, actionName, attributes)
}

module.exports = {
  /**
   * @param {ILoggerOptions} options
   */
  config: (options) => {
    _appName = options.appName ? options.appName : undefined
    _logFormat = options.logFormat ? options.logFormat : logFormat.msgJson
    _logCallback = options.logCallback ? options.logCallback : undefined
    _uuid = options.uuid ? uuid() : undefined
    _minLogLevel = toLogLevelNum(options.minLogLevel ? options.minLogLevel : defaultMinLogLevel)
    _logMaxTotalChars = options.logMaxTotalChars ? options.logMaxTotalChars : undefined
    _logTagFilter = options.logTagFilter ? options.logTagFilter : undefined
    _logTypeFilter = options.logTypeFilter ? options.logTypeFilter : undefined
    _logTypeDefaultGeneral = options.logTypeDefaultGeneral ? options.logTypeDefaultGeneral : false
    //
    _fatal = options.logFatalFn ? options.logFatalFn : consoleActual.error
    _error = options.logErrorFn ? options.logErrorFn : consoleActual.error
    _warn = options.logWarnFn ? options.logWarnFn : consoleActual.warn
    _info = options.logInfoFn ? options.logInfoFn : consoleActual.info
    _debug = options.logDebugFn ? options.logDebugFn : consoleActual.debug
  },
  /** @type {LogFunction} */
  fatal: (message, payload) => {
    logCall(logLevel.fatal, message, payload)
  },
  /** @type {LogFunction} */
  error: (message, payload) => {
    logCall(logLevel.error, message, payload)
  },
  /** @type {LogFunction} */
  warn: (message, payload) => {
    logCall(logLevel.warn, message, payload)
  },
  /** @type {LogFunction} */
  info: (message, payload) => {
    logCall(logLevel.info, message, payload)
  },
  /** @type {LogFunction} */
  debug: (message, payload) => {
    logCall(logLevel.debug, message, payload)
  },
  nr: newRelic,
  newRelic: newRelic,
  // Utils
  /**
   * @param {IObject} object
   */
  persist: (object) => {
    _persistent = { ..._persistent, ...object }
  },
  persistClear: () => {
    _persistent = {}
  },
  // Enums
  format: logFormat,
  level: logLevel,
  status: logStatus,
  type: logType,
}
