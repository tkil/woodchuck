# woodchuck

A simple logger for the browser and node.js

[![Version npm](https://img.shields.io/npm/v/woodchuck.svg?style=flat-square)](https://www.npmjs.com/package/woodchuck)

[![NPM](https://nodei.co/npm/woodchuck.png?downloads=true&downloadRank=true)](https://nodei.co/npm/woodchuck/)

#### Repo: https://gitlab.com/tkil/woodchuck

``` js
  import log from 'woodchuck';
```

## Summary
  `woodchuck` is a lightweight abstraction for your logging needs.  The goal of the project is to allow for a central logging layer for your app to interface with.  This separation of concerns allows for dev'ing so you can easily slot in a final logging implementation as time allows.  By default, the built in functions: console.debug, console.info, console.warn and console.error are used.  This can be changed and configured at any time as requirements change and needs arise.

## Basic Usage
Under the hood, `woodchuck` persists its config when imported to various modules.  No need for instantiation, simply import where needed and start logging.  It is recommended to configure options via the `log.config` methods in your app's startup / initialization section.

### Call signature
log.`<level>`(`<message>`, `<payload>`)\
or\
log.`<level>`(`<payload>`)

|  |  |
| ------------------- | ---- |
| `level`   | levels are: `DEBUG`, `INFO`, `WARN`, `ERROR` and `FATAL`; in order of severity
| `message` | message of the log, a string is encouraged, but the first param will except the payload)
| `payload` |  the payload object is free form, but keys `level`, `type`, `details`, and `data` are encouraged
|           | `level` - the severity of the log message
|           | `type` - the type of log, examples are init and api
|           | `details` - extra info and specific details
|           | `data` - useful for reporting request, response and arguments for later troubleshooting
|           | `error` - when invoking an error log, it is *STRONGLY* recomended that you attach the error to the payload - not the string message, but the actual error.  This is useful as it comes with a call stack
|           | `message` - message should be added if the first argument to log is the payload object, eg: `log.info({ message: 'My message', type: 'api' })`

### Examples

``` js
import { log } from 'woodchuck'

log.debug('My debug log!') // Must set minLogLevel to DEBUG or TRACE
// My debug log { level: 'DEBUG' }

log.info('My info log!')
// My info log! { level: 'INFO' }

log.warn('My warn log!')
// My warn log! { level: 'WARN' }

log.error('My error log!')
// My error log! { level: 'ERROR' }

log.fatal('My fatal log!')
// My fatal log! { level: 'FATAL' }
```

### Logging to New Relic
- Assumes window.newrelic.addPageAction is installed
``` js
log.nr('myFunc', 'myAction', { status: 'failure' })

// Output (stdout)
// NR: myAction @myFunc(), { status: 'failure' }
```

New Relic Output (in NR console)

| action   | functionName | status  | etc
| -------- | ------------ | ------- | -------
| myAction | myFunc       | failure | etc

## Configuration

To set configuration options: 
``` ts
log.config({
  appName: 'My Awesome App',
  uuid: true,
  format: 'json',
  minLogLevel: log.level.info
})
```

| Property              | Default         | Description
| --------------------- | --------------- | -----------------------------------------
| appName               | `undefined`     | Name of the application shown in the log |
| uuid                  | `false`         | show a UUID with the log
| logFormat             | `'msg-json'`    | 'msg-json', 'json'
| minLogLevel           | `'INFO'`        | Sets the minimum log when logging
| logMaxTotalChars      | `undefined`     | Sets a character limit when printing logs (min 100)
| logTypeFilter         | `undefined`     | Filter to only provided log types (note that the default is type 'general')
| logTypeDefaultGeneral | `false`         | Will default log type to `general` if no type is added to the payload
| logCallback           | `undefined`     | Callback function to be invoked whenever log is sent (note that payload.level is useful to drive behavior)
| logFatalFn               | `console.error` | Under the hood function to be used for fatal log calls
| logErrorFn               | `console.error` | Under the hood function to be used for error log calls
| logWarnFn                | `console.warn`  | Under the hood function to be used for warn log calls
| logInfoFn                | `console.info`  | Under the hood function to be used for info log calls
| logDebugFn               | `console.debug` | Under the hood function to be used for debug log calls


Example of wiring in the logCallback:
``` js
  import { log } from 'woodchuck';

  log.config.setLogCallback(() => {
    console.log('You called?')
  });
  log.info('Answer the call!')

  // Answer the call! { level: 'INFO' }  
  // You called?

  log.config.setLogCallback(({ userMessage }) => {
    myUserMessageToaster.toast(userMessage)
  });
  log.warn('WARN: Bad system error', { userMessage: 'Oops something broke on our end'})

  // WARN: Bad system error { level: 'WARN', { userMessage: 'Oops something broke on our end'} }  
  // ** myUserMessageToaster.toast() would be invoked with the callback's userMessage  
  // User would see: 'Oops something broke on our end' if you set things up properly
```

### Changelog
* v2.0.1 - Optimizes internals to lighten bundle, removes one off set configs to favor log.config({})
* v1.1.3 - Makes error optional on the payload object of log.warn
* v1.1.1 - Log as default export instead of prop of generic default export object
* v1.1.2 - Makes error optional on the payload object of log.error and log.fatal
* v1.0.0 - Bump to Release Version, Adds logging to new relic
