// Docs: https://jestjs.io/docs/en/configuration.html

module.exports = {
  clearMocks: true,
  collectCoverage: false,
  collectCoverageFrom: [
    '!**/lib/**'
  ],
  coverageDirectory: 'coverage',
  coveragePathIgnorePatterns: [
    '/node_modules/'
  ],
  globals: {
    'ts-jest': {
      tsConfig: 'tsconfig.json'
    }
  },
  moduleFileExtensions: [
    'ts',
    'tsx',
    'js',
    'json'
  ],
  moduleNameMapper: {
    '@modules/(.*)': '<rootDir>/node_modules/$1',
    '@utils/(.*)': '<rootDir>/src/utils/$1'
  },
  modulePathIgnorePatterns: [],
  setupFilesAfterEnv: [
    '<rootDir>/jest.setup.js'
  ],
  testMatch: [
    '**/index.test.tsx',
    '**/__tests__/*test.+(ts|tsx|js)',
    '**/tests/integration/*test.+(ts|tsx|js)'
  ],
  testPathIgnorePatterns: [
    '/node_modules/'
  ],
  transform: {
    '^.+\\.(ts|tsx)$': 'ts-jest'
  }
}
