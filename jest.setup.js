const consoleActual = { ...console }
console.debug = jest.fn((...args) => consoleActual.debug(...args))
console.info = jest.fn((...args) => consoleActual.info(...args))
console.warn = jest.fn((...args) => consoleActual.warn(...args))
console.error = jest.fn((...args) => consoleActual.error(...args))

afterEach(() => {
  console.debug.mockClear()
  console.info.mockClear()
  console.warn.mockClear()
  console.error.mockClear()
})
